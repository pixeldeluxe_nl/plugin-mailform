Als dit de eerste keer is dat je op een nieuwe pc of account via bitbucket een van onze plugins wilt updaten.
Moet je een ssh key aanmaken dit doe je op je bitbucket account bijvoorbeeld.
https://bitbucket.org/account/user/bobvanvelzen/ssh-keys/
Op deze pagina nadat je op add key klikt is een een linkje met een hele duidelijke uitleg die je door alle stappen loopt.



Plugin gebruik:
Als eerste moet je instellen naar wie de email's gaat dit doe je in het .env/general.php
Dit ziet er als volgt uit:
# SYSTEM EMAIL
# -------------------------------------------------------------------------------
SYSTEM_EMAIL="bob@pixeldeluxe.nl"

En in de general.php moet je systemEmail aan aliases toevoegen.
Dit komt in de alliases Zoals hier:
'aliases' => [
	'@baseUrl'            => getenv('BASE_URL'),
	'@basePath'           => getenv('ASSET_BASE_PATH'),
	'@systemEmail'        => getenv('SYSTEM_EMAIL'),
],

Hier is een voorbeeld van een formulier.

<form data-parsley-validate method="post" class="form-styles" id="contactForm" accept-charset="UTF-8">
	{{ csrfInput() }}
	<input type="hidden" name="action" value="mailform/sendmail">
	<input type="hidden" name="redirect" value="{{ siteUrl ~ 'lang_slug_contact'|t ~ '/' ~ 'lang_slug_success'|t }}">
	<input type="hidden" name="subject" value="Contact mail">

	<div class="row">
		<div class="col-12 col-md-8 offset-md-2">
			<div class="form-group">
				<input class="form-control" type="text" name="message[Naam]">
			</div> 
			
			<div class="form-group">
				<input class="form-control" id="from-email" type="email" name="message[E-mail]">
			</div>

			<div class="form-group">
				<input class="form-control" id="company" type="text" name="message[Bedrijfsnaam]" value="">
			</div>
			
			<div class="form-group">
				<input class="form-control phone" id="phone" type="tel" name="message[Telefoonnummer]" value="">
			</div>
		</div>
	</div>
</form>

Zoals je hierboven kunt zien zijn er een aantal hidden input fields nodig
de action die de plugin functie aanroep
de redirect hidden input de url die hierin staat is waar de user heen gestuurd word na het verzenden van de E-mail.
En subject daaronder spreekt voor zich dat is het mail onderwerp.

Nu is er nog een belangrijk ding de benaming van de velden alle velden moeten als naam message[] hebben
je kan vervolgens tussen de haakjes "[]" een naam typen zoals in het voorbeeld hierboven