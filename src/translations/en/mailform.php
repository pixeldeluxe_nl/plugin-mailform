<?php
/**
 * mailform plugin for Craft CMS 3.x
 *
 * send emals
 *
 * @link      www.pixeldeluxe.nl
 * @copyright Copyright (c) 2018 Pixeldeluxe
 */

/**
 * @author    Pixeldeluxe
 * @package   Mailform
 * @since     1.0.0
 */
return [
    'mailform plugin loaded' => 'mailform plugin loaded',
];
