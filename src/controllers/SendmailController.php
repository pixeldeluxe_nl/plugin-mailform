<?php
/**
 * mailform plugin for Craft CMS 3.x
 *
 * send emals
 *
 * @link      www.pixeldeluxe.nl
 * @copyright Copyright (c) 2018 Pixeldeluxe
 */

namespace pixeldeluxe\mailform\controllers;

use pixeldeluxe\mailform\Mailform;

use Craft;
use craft\web\Controller;
use yii\web\Response;
use craft\mail\Message;
use craft\base\Component;
use craft\web\View;
use GuzzleHttp;

/**
 * @author    Pixeldeluxe
 * @package   Mailform
 * @since     1.0.0
 */
class SendmailController extends Controller
{

    // Protected Properties
    // =========================================================================

    public $allowAnonymous = true;

    // Public Methods
    // =========================================================================

    /**
     * @return mixed
     */
    public function actionIndex()
    {   
        // alle variabelen ophalen uit het post request
        $this->requirePostRequest();
        $request = Craft::$app->getRequest();
        $toEmail = Craft::getAlias('@systemEmail');
        $secretKey = Craft::getAlias('@recaptchaSecretKey');

        // de variabelen ophalen om een recaptcha verificatie te doen
        $captcha = '';
        if ($_POST['g-recaptcha-response']) {
            $captcha = $request->getBodyParam('g-recaptcha-response');
        }

        $base = "https://www.google.com/recaptcha/api/siteverify";
        $params = array(
            'secret' =>  $secretKey,
            'response' => $captcha
        );

        // de verificatie naaar google versturen
        $client = new GuzzleHttp\Client();
        $response = $client->request('POST', $base, ['form_params' => $params]);

        // analiseren wat google terug stuurt
        if($response->getStatusCode() == 200)
        {
            $json = json_decode($response->getBody());

            if($json->success)
            {
                // email opbouwen
                $emailFields = $request->getBodyParam('message');
                $body = '';
                if ($emailFields != null)
                {
                    foreach ($emailFields as $key => $value) {
                        $body = $body . $key . ': ' . $value . "<br />";
                    }
                }
                
                $message = new Message();
                $message->setFrom($toEmail);
                $message->setTo($toEmail);
                $message->setSubject($request->getBodyParam('subject'));
                $message->setHtmlBody($body);

                if (!empty($attachments) && \is_array($attachments)) {

                    foreach ($attachments as $fileId) {
                        if ($file = Craft::$app->assets->getAssetById((int)$fileId)) {
                            $message->attach($this->getFolderPath() . '/' . $file->filename, array(
                                'fileName' => $file->title . '.' . $file->getExtension()
                            ));
                        }
                    }
                }

                // email verzenden
                $result = Craft::$app->mailer->send($message);

                // bij een fout bij het verzenden van de email een melding laten zien.
                if ($result == 0)
                {
                    Craft::$app->session->setFlash('mailError', "Er is iets fout gegaan het formulier is niet verzonden");
                }
                else 
                {
                    // SUCCES als er een redirect input bestaat hierheen redirecten en anders een succes message laten zien
                    $redirectUrl = $request->getBodyParam('redirect');
                    if ($redirectUrl != null) {
                        return \Craft::$app->controller->redirect($redirectUrl);
                    }
                    else 
                    {
                        Craft::$app->session->setFlash('mailError', "Uw bericht is succesvol verzonden.");
                        return true;
                    }
                }

                return 'succes';
            } else {
                // niet geverifieerd
                craft()->userSession->setError(Craft::t('Deze email is niet verzonden.'));
                return false;
            }

        } else {
            // geen response aangekomen.
            echo "false";
            craft()->userSession->setError(Craft::t('Deze email is niet verzonden.'));
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function actionDoSomething()
    {
        $result = 'Welcome to the DefaultController actionDoSomething() method';

        return $result;
    }
}
