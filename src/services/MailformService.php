<?php
/**
 * mailform plugin for Craft CMS 3.x
 *
 * send emals
 *
 * @link      www.pixeldeluxe.nl
 * @copyright Copyright (c) 2018 Pixeldeluxe
 */

namespace pixeldeluxe\mailform\services;

use pixeldeluxe\mailform\Mailform;

use Craft;
use craft\base\Component;

/**
 * @author    Pixeldeluxe
 * @package   Mailform
 * @since     1.0.0
 */
class MailformService extends Component
{
    // Public Methods
    // =========================================================================

    /*
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }
}
